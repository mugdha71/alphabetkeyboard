package xyz.tigerdigital.alphabetkeyboard;

/**
 * Created by mugdha on 8/28/18.
 */

public class StaticEmojiCode {
    public static boolean isDARK = true;

    public static boolean isIsDARK() {
        return isDARK;
    }

    public static String getEmoji(int code)
    {
        String res = "";
        switch (code){
            case -1000:
                res = "\uD83D\uDE04";
                break;
            case -1001:
                res = "\uD83D\uDE03";
                break;
            case -1002:
                res = "\uD83D\uDE00";
                break;
            case -1003:
                res = "\uD83D\uDE0A";
                break;
            case -1004:
                res = "\u263A";
                break;
            case -1005:
                res = "\uD83D\uDE09";
                break;
            case -1006:
                res = "\uD83D\uDE0D";
                break;
            case -1007:
                res = "\uD83D\uDE18";
                break;
            case -1008:
                res = "\uD83D\uDE1A";
                break;
            case -1009:
                res = "\uD83D\uDE17";
                break;
            case -1010:
                res = "\uD83D\uDE19";
                break;
            case -1011:
                res = "\uD83D\uDE1C";
                break;
            case -1012:
                res = "\uD83D\uDE1D";
                break;
            case -1013:
                res = "\uD83D\uDE1B";
                break;
            case -1014:
                res = "\uD83D\uDE33";
                break;
            case -1015:
                res = "\uD83D\uDE01";
                break;
            case -1016:
                res = "\uD83D\uDE14";
                break;
            case -1017:
                res = "\uD83D\uDE0C";
                break;
            case -1018:
                res = "\uD83D\uDE12";
                break;
            case -1019:
                res = "\uD83D\uDE1E";
                break;
            case -1020:
                res = "\uD83D\uDE23";
                break;
            case -1021:
                res = "\uD83D\uDE22";
                break;
            case -1022:
                res = "\uD83D\uDE02";
                break;
            case -1023:
                res = "\uD83D\uDE2D";
                break;
            case -1024:
                res = "\uD83D\uDE2A";
                break;
            case -1025:
                res = "\uD83D\uDE25";
                break;
            case -1026:
                res = "\uD83D\uDE30";
                break;
            case -1027:
                res = "\uD83D\uDE05";
                break;
            case -1028:
                res = "\uD83D\uDE13";
                break;
            case -1029:
                res = "\uD83D\uDE29";
                break;
            case -1030:
                res = "\uD83D\uDE2B";
                break;
            case -1031:
                res = "\uD83D\uDE28";
                break;
            case -1032:
                res = "\uD83D\uDE31";
                break;
            case -1033:
                res = "\uD83D\uDE20";
                break;
            case -1034:
                res = "\uD83D\uDE21";
                break;
            case -1035:
                res = "\uD83D\uDE24";
                break;
            case -1036:
                res = "\uD83D\uDE16";
                break;
            case -1037:
                res = "\uD83D\uDE06";
                break;
            case -1038:
                res = "\uD83D\uDE0B";
                break;
            case -1039:
                res = "\uD83D\uDE37";
                break;
            case -1040:
                res = "\uD83D\uDE0E";
                break;
            case -1041:
                res = "\uD83D\uDE34";
                break;
            case -1042:
                res = "\uD83D\uDE35";
                break;
            case -1043:
                res = "\uD83D\uDE32";
                break;
            case -1044:
                res = "\uD83D\uDE1F";
                break;
            case -1045:
                res = "\uD83D\uDE26";
                break;
            case -1046:
                res = "\uD83D\uDE27";
                break;
            case -1047:
                res = "\uD83D\uDE08";
                break;
            case -1048:
                res = "\uD83D\uDC7F";
                break;
            case -1049:
                res = "\uD83D\uDE2E";
                break;

        }
        return res;
    }
}
