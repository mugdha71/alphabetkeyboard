package xyz.tigerdigital.alphabetkeyboard;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.NinePatchDrawable;
import android.inputmethodservice.Keyboard;
import android.util.AttributeSet;

import java.util.List;

import static xyz.tigerdigital.alphabetkeyboard.AlphabetKeyboard.isDark;

public class MyKeyboardView extends android.inputmethodservice.KeyboardView {

    Context context;
    Paint paint,paintText;
    Rect bounds ;
    String text = "Alphabet Keyboard";
    int textSize = 50;
//

    public MyKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        this.context = context;
        paint = new Paint();
        paintText = new Paint();
        bounds= new Rect();
        paintText.setColor(getResources().getColor(R.color.keyboard_font_color));
//        paintText.setColor(Color.BLACK);
        paint.setColor(getResources().getColor(R.color.keyboard_background_color));

        paintText.setTextSize(textSize);
        paintText.getTextBounds(text,0,text.length(),bounds);
        paintText.setFakeBoldText(true);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

//          Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Hippie.otf");
//        paint.setTypeface(font);

        if(isDark)
        {
            paintText.setColor(getResources().getColor(R.color.keyboard_background_color_dark));
            paint.setColor(getResources().getColor(R.color.keyboard_background_color));
        }
        else
        {
            paintText.setColor(getResources().getColor(R.color.keyboard_background_color));
            paint.setColor(getResources().getColor(R.color.keyboard_background_color_dark));
        }
        List<Keyboard.Key> keys = getKeyboard().getKeys();
        for (Keyboard.Key key : keys) { // int i = 0 ; switch(i) and implement your logic
            if(key.label!=null)
            {
                if (key.label.toString().equalsIgnoreCase("Alphabet Keyboard")) {
                    canvas.drawRoundRect(key.x+20, key.y+20,key.x+ key.width-20 , key.y+key.height-20,key.width/15,key.width/15, paint);
                    while (bounds.width()>(key.width-80))
                    {
                        paintText.setTextSize(textSize--);
                        paintText.getTextBounds(text,0,text.length(),bounds);

                    }
                    canvas.drawText("Alphabet Keyboard", key.x+key.width/2-bounds.width()/2, key.y+key.height/2+20 ,paintText); //x=300,y=300
                }
            }

        }
    }
}