package xyz.tigerdigital.alphabetkeyboard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;

import java.util.List;

import static android.content.ContentValues.TAG;
import static xyz.tigerdigital.alphabetkeyboard.StaticEmojiCode.isDARK;


public class AlphabetKeyboard extends InputMethodService implements KeyboardView.OnKeyboardActionListener
{

    private MyKeyboardView keyboardView;
    private Keyboard keyboard;

    private boolean isCaps = false;
    private boolean standardKeyboardID;
    private boolean numberMode = false;
    private boolean numbers_sym2 = false;
    RidmikParser parser = new RidmikParser();
    String word = "",bangla = "";
    boolean isBangla = true;
    private List<Keyboard.Key> keys;
    SharedPreferences sp;
    private View view = null;
    public static boolean isDark = false;
    Context context;

    @Override
    public View onCreateInputView()
    {
        context = this;
        sp = getSharedPreferences(getPackageName(), Activity.MODE_PRIVATE);

        LayoutInflater inflater1 = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(!sp.getBoolean("theme", false)) {
            isDark=false;
            view = inflater1.inflate(R.layout.keyboard, null);
        }
        else {
            isDark = true;
            view = inflater1.inflate(R.layout.keyboard_dark, null);
            view.findViewById(R.id.linear_layout).setBackgroundColor(Color.LTGRAY);
        }
        view.invalidate();
        keyboardView = view.findViewById(R.id.keyboard);
        ImageButton emoji = view.findViewById(R.id.emoji_button);
        emoji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keyboard = new Keyboard(context, R.xml.emoji);//////////////////////////////add emoji symbol
                keyboardView.setKeyboard(keyboard);
                standardKeyboardID = false;
            }
        });
        Switch changeTheme = view.findViewById(R.id.change_theme);
        changeTheme.setChecked(sp.getBoolean("theme", false));
        changeTheme.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                sp.edit().putBoolean("theme",!sp.getBoolean("theme", false)).apply();
                setInputView(onCreateInputView());
            }
        });
//        changeTheme.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                sp.edit().putBoolean("theme",!sp.getBoolean("theme", false)).apply();
//                setInputView(onCreateInputView());
//            }
//        });
//        if(!sp.getBoolean("theme", false))keyboardView = (MyKeyboardView) getLayoutInflater().inflate(R.layout.keyboard, null);
//        else keyboardView = (MyKeyboardView) getLayoutInflater().inflate(R.layout.keyboard_dark, null);
        if (numberMode) {   // number mode on
            if(numbers_sym2) {
                keyboard = new Keyboard(this, R.xml.numbers_symbols2);
                keyboardView.setKeyboard(keyboard);
                getShiftKey(keyboard).icon = ContextCompat.getDrawable(this, R.drawable.ic_shift_key);
                keyboardView.invalidateAllKeys();
            }
            else {
                keyboard = new Keyboard(this, R.xml.number_symbols_shifted); ///////////////////////////////// add number symbol shifted
                getShiftKey(keyboard).icon = ContextCompat.getDrawable(this, R.drawable.ic_shift_key_active);
                keyboardView.setKeyboard(keyboard);
                keyboardView.invalidateAllKeys();
            }
        }
        else {
            keyboard = new Keyboard(this, R.xml.qwerty_en);
            keyboardView.setKeyboard(keyboard);
            standardKeyboardID = true;
            keyboard.setShifted(isCaps);
            if (isBangla) getLanguageKey(keyboard).label = "BN";
            else getLanguageKey(keyboard).label = "EN";
            if(isCaps)
                getShiftKey(keyboard).icon = ContextCompat.getDrawable(this, R.drawable.ic_shift_key_active);
            else
                getShiftKey(keyboard).icon = ContextCompat.getDrawable(this, R.drawable.ic_shift_key);

            keyboardView.invalidateAllKeys();
        }
        keyboardView.setOnKeyboardActionListener(this);
        return view;
    }

    @Override
    public void onPress(int primaryCode)
    {

    }

    @Override
    public void onRelease(int primaryCode)
    {

    }

    Keyboard.Key getShiftKey(Keyboard keyboard)
    {
        for (Keyboard.Key key:keyboard.getKeys()){
            if(key.codes != null){
                if (key.codes[0] == -1)
                    return key;
            }
        }
        return null;
    }
    Keyboard.Key getLanguageKey(Keyboard keyboard)
    {
        for (Keyboard.Key key:keyboard.getKeys()){
            if(key.codes != null){
                if (key.codes[0] == -6)
                    return key;
            }
        }
        return null;
    }
    @Override
    public void onKey(int primaryCode, int[] keyCodes)
    {
        Log.e("onKey","onKey");
        InputConnection inputConnection = getCurrentInputConnection();



        switch (primaryCode)
        {
            case -6:
                keys = keyboardView.getKeyboard().getKeys();
                for (Keyboard.Key key:keys){
                    if(key.label != null){
                        if(key.label.toString().equalsIgnoreCase("BN"))
                        {
                            key.label = "EN";
                            isBangla = false;
                        }
                        else if(key.label.toString().equalsIgnoreCase("EN"))
                        {
                            key.label = "BN";
                            isBangla = true;
                        }

                    }
                }

                word = "";
                bangla = "";
                break;
            case -3:
                inputConnection.commitText(String.valueOf("\u09F3"), 1);
                break;
            case -19:
                sp.edit().putBoolean("theme",!sp.getBoolean("theme", false)).apply();
                setInputView(onCreateInputView());
                break;
            case -5000:
                keyboard = new Keyboard(this, R.xml.qwerty_en);//////////////////////////////add emoji symbol
                keyboardView.setKeyboard(keyboard);
                standardKeyboardID = true;
                break;
            case Keyboard.KEYCODE_DELETE:
                inputConnection.deleteSurroundingText(1, 0);
                word = removeLastChar(word);
                break;
            case Keyboard.KEYCODE_SHIFT:
                if (numberMode) {   // number mode on
                    if(numbers_sym2) {
                        keyboard = new Keyboard(this, R.xml.numbers_symbols2);
                        keyboardView.setKeyboard(keyboard);
                        getShiftKey(keyboard).icon = ContextCompat.getDrawable(this, R.drawable.ic_shift_key);
                        keyboardView.invalidateAllKeys();
                    }
                    else {
                        keyboard = new Keyboard(this, R.xml.number_symbols_shifted); ///////////////////////////////// add number symbol shifted
                        getShiftKey(keyboard).icon = ContextCompat.getDrawable(this, R.drawable.ic_shift_key_active);
                        keyboardView.setKeyboard(keyboard);
                        keyboardView.invalidateAllKeys();
                    }
                    numbers_sym2 = !numbers_sym2;
                }
                else {
                    isCaps = !isCaps;
                    keyboard.setShifted(isCaps);
                    if(isCaps)
                        getShiftKey(keyboard).icon = ContextCompat.getDrawable(this, R.drawable.ic_shift_key_active);
                    else
                        getShiftKey(keyboard).icon = ContextCompat.getDrawable(this, R.drawable.ic_shift_key);

                    keyboardView.invalidateAllKeys();
                }
                break;
            case Keyboard.KEYCODE_DONE:
                inputConnection.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                break;
            case Keyboard.KEYCODE_MODE_CHANGE:
                if (standardKeyboardID)
                {
                    if(numbers_sym2)
                    {
                        keyboard = new Keyboard(this, R.xml.number_symbols_shifted);
                        getShiftKey(keyboard).icon = ContextCompat.getDrawable(this, R.drawable.ic_shift_key_active);
                    }
                    else keyboard = new Keyboard(this, R.xml.numbers_symbols2);
                    keyboardView.setKeyboard(keyboard);
                    standardKeyboardID = !standardKeyboardID;
                    isBangla = false;
                }
                else
                {
                    keyboard = new Keyboard(this, R.xml.qwerty_en);
                    keyboardView.setKeyboard(keyboard);
                    keyboardView.setShifted(isCaps);
                    if (isBangla) getLanguageKey(keyboard).label = "BN";
                    else getLanguageKey(keyboard).label = "EN";
                    if(isCaps)getShiftKey(keyboard).icon = ContextCompat.getDrawable(this, R.drawable.ic_shift_key_active);
                    standardKeyboardID = !standardKeyboardID;
                }
                numberMode = !numberMode;
                break;

            default:
                Log.d(TAG, "onKey: Unicode "+primaryCode);
                char code = (char) primaryCode;
                if (Character.isLetter(code) && isCaps)
                    code = Character.toUpperCase(code);
                if(primaryCode<-999)
                {
                    inputConnection.commitText(StaticEmojiCode.getEmoji(primaryCode), 1);
                }
                else if(isBangla) {
                    if (code == ' ') {
                        word = "";
                        bangla = "";
                        inputConnection.commitText(" ", 1);
                    } else if (code == '.') {
                        word = "";
                        bangla = "";
                        inputConnection.commitText("\u09F7", 1);
                    } else {
                        inputConnection.deleteSurroundingText(bangla.length(), 0);
                        word += code;
                        bangla = parser.toBangla(word);
                        inputConnection.commitText(bangla, 1);
                    }
                    Log.d(TAG, "onKey: " + word + " " + bangla);
                }
                else inputConnection.commitText(String.valueOf(code), 1);
        }


    }

    @Override
    public void onText(CharSequence text)
    {

    }
    private static String removeLastChar(String str) {
        if(str.length()>1)
            return str.substring(0, str.length() - 1);
        else return "";
    }
    public void swipeLeft() {
        /****
         * payment implement
         */
    }

    @Override
    public void swipeRight()
    {

    }

    @Override
    public void swipeDown()
    {

    }

    @Override
    public void swipeUp()
    {

    }



    //
    @Override
    public void onFinishInputView(boolean finishingInput)
    {
        isCaps = false;
        keyboard.setShifted(isCaps);
        keyboardView.invalidateAllKeys();

        keyboard = new Keyboard(this, R.xml.qwerty_en);
        keyboardView.setKeyboard(keyboard);
        standardKeyboardID = true;

        super.onFinishInputView(finishingInput);
    }
}
