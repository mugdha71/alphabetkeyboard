package xyz.tigerdigital.alphabetkeyboard;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import xyz.tigerdigital.alphabetkeyboard.R;

public class EnableSettings extends AppCompatActivity {
    ImageView enable_icon;
    EditText testEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enable_settings);
        enable_icon = findViewById(R.id.enable_icon);
        testEdit = findViewById(R.id.testEdit);
        if(isEnabled())
            enable_icon.setImageDrawable(getDrawable(R.drawable.ic_ok));
        else enable_icon.setImageDrawable(getDrawable(R.drawable.ic_error));
    }

    boolean isEnabled()
    {
        InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
        assert imeManager != null;
        for(InputMethodInfo i : imeManager.getEnabledInputMethodList())
        {
            if(i.getPackageName().equalsIgnoreCase(getPackageName()))
                return true;
        }
        return false;
    }
    public void switch_keyboard(View view) {
        InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
        assert imeManager != null;
        imeManager.showInputMethodPicker();
    }

    public void enable_keyboard(View view) {
        Intent intent = new Intent(Settings.ACTION_INPUT_METHOD_SETTINGS);
        startActivityForResult(intent,54);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 54)
        {
            if(isEnabled())enable_icon.setImageDrawable(getDrawable(R.drawable.ic_ok));
            else enable_icon.setImageDrawable(getDrawable(R.drawable.ic_error));
        }
    }

    public void clear_text(View view) {
        testEdit.setText("");
    }
}
